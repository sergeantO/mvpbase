﻿using System;
using MVP.Presentation.Common;

namespace MVP.Presentation.Views
{
    public interface ILoginView : IView
    {
        string Username { get; }
        string Password { get; }
        event Action Login;
    }
}